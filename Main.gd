extends Sprite


const act_one = preload("res://assets/dialog/act-01.tres")
const act_two = preload("res://assets/dialog/act-02.tres")
const act_three = preload("res://assets/dialog/act-03.tres")
const act_four = preload("res://assets/dialog/act-04.tres")
const act_five = preload("res://assets/dialog/act-05.tres")
const act_six = preload("res://assets/dialog/act-06.tres")
const end = preload("res://assets/dialog/end.tres")

var acts = [act_one, act_two, act_three, act_four, act_five, act_six, end]

onready var player = $"%Player"
onready var dialogue = $"%Dialogue"
onready var title = $"%Title"

var __title = true
var __act = 0

func _ready() -> void:
	player.connect("collected", self, "_on_player_collected")
	dialogue.connect("dialogue_finished", self, "_on_dialogue_finished")


func _input(event) -> void:
	if __title and event is InputEventKey:
		if event.scancode == KEY_UP and event.is_pressed() and not event.is_echo():
			__title = false
			title.hide()
			dialogue.show()
			dialogue.play_dialogue(acts[__act])
			get_tree().set_input_as_handled()
	


func _on_player_collected() -> void:
	dialogue.show()
	__act += 1
	if __act < acts.size():
		dialogue.play_dialogue(acts[__act])
	


func _on_dialogue_finished() -> void:
	if __act < acts.size() - 1:
		player.next_act()
		dialogue.hide()
