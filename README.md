# The Consuming Dark

A short horror short about a cave exploring looking for a ray
of hope in the crushing darkness.


## FOSS Tools Used:
- Fedora Linux
- [Pixelorama (MIT)](https://github.com/Orama-Interactive/Pixelorama)
- [Godot (MIT)](https://godotengine.org/)
- [Godot Dialogue Manager (MIT)](https://github.com/nathanhoad/godot_dialogue_manager)
- [Godot Pixelorama Importer (MIT)](https://github.com/Technohacker/godot_pixelorama_importer)
- [Kanban Task (MIT)](https://github.com/HolonProduction/godot_kanban_tasks)

## License
Art  - CC-BY-SA
Sound - CC-BY-SA
Code  - MIT