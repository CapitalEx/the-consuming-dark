extends PanelContainer
signal dialogue_finished
signal next_pressed

const DialogueLine := preload("res://addons/dialogue_manager/dialogue_line.gd")
const CHAR_RATE = 30.0

onready var text : RichTextLabel = $MarginContainer/CurrentText

var __running := false
var __tween : SceneTreeTween

func play_dialogue(dialogue: DialogueResource) -> void:
	text.visible_characters = 0
	__running = true
	
	var line : DialogueLine = yield(DialogueManager.get_next_dialogue_line("intro", dialogue), "completed")
	
	while line:
		var text_len = len(line.dialogue)
		var duration = text_len / CHAR_RATE
		__tween    = get_tree().create_tween()
		
		text.bbcode_text = line.dialogue
		text.visible_characters = 0
		
		__tween.tween_property(text, "visible_characters", text_len, duration)
		
		yield(__tween, "finished")
		
		yield(self, "next_pressed")
		
		line = yield(DialogueManager.get_next_dialogue_line(line.next_id, dialogue), "completed")
	
	emit_signal("dialogue_finished")
	__running = false

func _input(event) -> void:
	if event is InputEventKey and __running:
		if event.scancode == KEY_UP and event.is_pressed() and not event.is_echo():
			if __tween.is_valid():
				__tween.set_speed_scale(100)
			get_tree().set_input_as_handled()
			emit_signal("next_pressed")
			
