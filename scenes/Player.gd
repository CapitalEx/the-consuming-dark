extends Sprite
signal collected

const ANIMATIONS = ["act-1", "act-2", "act-3", "act-4", "act-5", "end"]

var __act = -1
var __steps = 5

func _ready() -> void:
	pass

func next_act() -> void:
	__steps = 5
	__act += 1
	position.y = 147
	if __act < ANIMATIONS.size():
		$AnimationPlayer.current_animation = ANIMATIONS[__act]

func _unhandled_input(event) -> void:
	if event is InputEventKey and __act < ANIMATIONS.size():
		if event.scancode == KEY_UP and not event.is_echo() and event.is_pressed():
			position += Vector2.UP * 24
			__steps -= 1
			if __steps == 0:
				emit_signal("collected")
